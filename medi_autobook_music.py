#!/usr/local/bin/python3.7

import requests
import argparse
import json
import datetime
from bs4 import BeautifulSoup
import time
from pygame import mixer

# extra variables ------------------
debug = False
debug_content = False
http_med_session = requests.Session()
http_med_session.headers.update({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'})
# ----------------------------------

def show_debug(session_response):
    if session_response.status_code != 200:
        print("\t * ERR CODE: " +  str(session_response.status_code))
        exit(1)
    if debug == True:
        if session_response.history:
            for histreq in session_response.history:
                print("\tPrevious status   :\t {}".format(histreq.status_code))
                print("\tPrevious URL      :\t {}".format(histreq.url))
                print("\tPrevious Headers  :\t {}".format(histreq.headers))
                if debug_content == True:
                    print("\tPrevious Content  :\t {}".format(histreq.text))
                print("\n")
        print("URL                    :\t {}".format(session_response.request.url))
        print("URL method             :\t {}".format(session_response.request.method))
        print("Request headers        :\t {}".format(session_response.request.headers))
        print("Previous status code   :\t {}".format(session_response.history))
        print("Current status code    :\t {}".format(session_response.status_code))
        print("Current http headers   :\t {}".format(session_response.headers))
        if debug_content == True:
            print("Page content           :\t {}".format(session_response.text))
        print("-------------------------------------------------------------------\n\n")


def authentication(login, password):
    login_url = [
        "https://mol.medicover.pl/Users/Account/AccessDenied?ReturnUrl=%2F",
        "https://mol.medicover.pl/Users/Account/LogOn?ReturnUrl=%2F"
    ]

    print("* INF: Login phase 1")
    r0 = http_med_session.get(login_url[0], allow_redirects=True)
    show_debug(r0)

    print("* INF: Login phase 2 - obtain CSRF Token")
    r1 = http_med_session.get(login_url[1], allow_redirects=True)
    show_debug(r1)
    login_url.append(r1.url)

    soup = BeautifulSoup(r1.text, 'html.parser')
    csrf_token = str(soup.script)[217:308] # i know ugly
    print("* INF: Login phase 2 - CSRF Token: " + csrf_token)

    print("* INF: Login phase 3 - Send username and password")
    r2 = http_med_session.post(login_url[2], data={"idsrv.xsrf":csrf_token, "username": login, "password": password}, allow_redirects=True)
    show_debug(r2)

    print("* INF: Login phase 4 - Parse forms from page")
    form_with_hidden_data = {}
    soup = BeautifulSoup(r2.content,'html.parser')
    for inputs in soup.find_all('input'):
        form_with_hidden_data.update({inputs['name']:inputs['value']})

    if debug == True:
        print(form_with_hidden_data)
        print("\n\n")

    return form_with_hidden_data


def open_my_visits(form_with_hidden_data):
    visits_url = [
        "https://mol.medicover.pl/Medicover.OpenIdConnectAuthentication/Account/OAuthSignIn",
        "https://mol.medicover.pl/MyVisits",
        "https://mol.medicover.pl/MyVisits?bookingTypeId=2&specializationId=9&pfm=1",
        "https://mol.medicover.pl/MyVisits?regionId=202&bookingTypeId=2&specializationId=9&serviceId=&clinicId=-1&languageId=-1&doctorId=-1&searchSince=2018-10-02T02%3A00%3A00.000Z&periodOfTheDay=0&isSetBecauseOfPcc=false&isSetBecausePromoteSpecialization=false&selectedSpecialties="]

    print("* INF: Confirm oauth tokens and enter Main Page")
    r4 = http_med_session.post(visits_url[0], data=form_with_hidden_data, allow_redirects=True)
    show_debug(r4)

    print("* INF: Open MyVisits page")
    r5 = http_med_session.get(visits_url[1], allow_redirects=True)
    show_debug(r5)

    soup = BeautifulSoup(r5.content,'html.parser')
    spans = soup.find_all('span',attrs={'class':'username'})
    if spans[1].string != "":
        print("* INF: You are logged as {}".format(spans[1].string))
    else:
        print("* ERR: None user first and last name")
        exit(1)

    print("* INF: Open Booking for specialist")
    r6 = http_med_session.get(visits_url[2], allow_redirects=True)
    show_debug(r6)

    print("* INF: Open Search Page")
    r7 = http_med_session.get(visits_url[3], allow_redirects=True)
    show_debug(r7)


def search_visit(start_date, end_date, after_hour, before_hour, medical_specialization):
    visits_table = []
    acctual_date = datetime.datetime.now()
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S")
    after_hour = datetime.datetime.strptime(after_hour,"%H")
    before_hour = datetime.datetime.strptime(before_hour,"%H")

    if (debug == True):
        print("Acctual date: {}".format(acctual_date))
        print("Start date: {}".format(start_date))
        print("End date: {}".format(end_date))

    if (acctual_date > start_date):
        start_date = acctual_date + datetime.timedelta(minutes=30)
        if (debug == True):
            print("Start date not acctual, correcting to: {}".format(str(start_date.strftime("%Y-%m-%dT%H:%M:%S"))))

    if (acctual_date > end_date):
        print("Sorry, it`s already after end_date")
        return 0


    response = http_med_session.post("https://mol.medicover.pl/api/MyVisits/SearchFreeSlotsToBook?language=pl-PL",
                                     data={"regionId": "202",
                                "bookingTypeId": "2",
                                "specializationId": medical_specialization,
                                "clinicId": "27944",
                                "languageId": "-1",
                                "doctorId": "-1",
                                "searchSince": start_date.strftime("%Y-%m-%dT%H:%M:%S"),
                                "searchForNextSince": "null",
                                "periodOfTheDay": "0",
                                "isSetBecauseOfPcc": "false",
                                "isSetBecausePromoteSpecialization": "false"},
                                     headers={"Accept": "application/json"}
                                     )
    show_debug(response)
    response_json = json.loads(response.text)

    for x in (response_json['items']):
        id_wizyty = x['id']
        appointmentDate = x['appointmentDate']
        clinicname = x['clinicName']
        doctorname = x['doctorName']

        kiedy_wizyta = datetime.datetime.strptime(appointmentDate, "%Y-%m-%dT%H:%M:%S")
        if (kiedy_wizyta < end_date) and (kiedy_wizyta.time() >= after_hour.time()) and (kiedy_wizyta.time() <= before_hour.time()):
            visits_table.append({'id':id_wizyty, 'date': appointmentDate, 'place': clinicname, 'doctor': doctorname})
            if debug == True:
                print("\t FOUND APPOINTMENT DATE: {}".format(appointmentDate))

    return visits_table


def book_visit(visit):
    response_process = http_med_session.get("https://mol.medicover.pl/MyVisits/Process/Process?id=" + visit['id'], allow_redirects=True)
    show_debug(response_process)

    # parse response and search visit id
    soup = BeautifulSoup(response_process.content,'html.parser')
    form_to_confirm = {}

    for inputs in soup.find_all('input'):
        form_to_confirm.update({inputs['name']:inputs['value']})

    response_confirm = http_med_session.post("https://mol.medicover.pl/MyVisits/Process/Confirm",data=form_to_confirm,allow_redirects=True)
    show_debug(response_confirm)


# ------------------------------------------
# MAIN PROGRAM -----------------------------
# ------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--login", help="Your medicover login", required=True)
    parser.add_argument("--password", help="Your medicover password", required=True)
    parser.add_argument("--date_from", help="Enter the start date of the search period e.g 2018-09-10", required=True)
    parser.add_argument("--date_to", help="Enter the end date of the search period e.g 2018-09-12", required=True)
    parser.add_argument("--specialization", help="Doctor specialization (look README)", required=True)
    parser.add_argument("--after_hour", help="Search visits after an hour", required=False)
    parser.add_argument("--before_hour", help="Search visits before an hour", required=False)
    parser.add_argument("--check_interval", help="Sleep between searches", required=False)
    args = parser.parse_args()
    args.date_to = args.date_to+"T23:59:59"
    args.date_from = args.date_from+"T00:00:00"

    # check agruments and set default values
    if (args.login) and (args.password) and (args.date_from) and (args.date_to) and (args.specialization):
        # set after hour if not defined
        if not(args.after_hour):
            args.after_hour = 8

        # set before hour if not defined
        if not(args.before_hour):
            args.before_hour = 22

        # set default interval when not defined
        if not(args.check_interval):
            args.check_interval = 30

        # enter main page and log in
        print("--------------------------------------------------")
        print("Log in to the systems ...\n ")
        hidden_tokens = authentication(args.login, args.password)

        # open my visits bookmark
        print("--------------------------------------------------")
        print("Navigate to booking page ...\n ")
        open_my_visits(hidden_tokens)

        # search visits in a loop (check interval)
        print("--------------------------------------------------")
        print("Search for visits according to your preference ...\n ")
        while True:
            # request to api and return possible visits
            found_visit_list = search_visit(args.date_from, args.date_to, str(args.after_hour), str(args.before_hour), args.specialization)
            print('.', flush=True, end='')

            # quit when not found and we are after end_date
            if (found_visit_list == 0):
                break

            # normal search
            if (found_visit_list):
                print("\nBooking:\n",
                      "\t Date: {}\n".format(found_visit_list[0]['date']),
                      "\t Doctor: {}\n".format(found_visit_list[0]['doctor']),
                      "\t Place: {}\n".format(found_visit_list[0]['place']))
                book_visit(found_visit_list[0])
                mixer.init()
                mixer.music.load('/home/pyton/tas_red_alert.mp3')
                mixer.music.play()
                print("play music")
                time.sleep(300)
                break
            # interval between searches
            time.sleep(int(args.check_interval))

    else:
        print("Please provide all arguments. Use -h or --help")

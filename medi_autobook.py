#!/usr/local/bin/python3.7
from __future__ import print_function, unicode_literals

import argparse
import datetime
import json
import sys
import time

import regex
import requests
from PyInquirer import prompt, Validator, ValidationError
from bs4 import BeautifulSoup
from pytz import timezone

# hard-coded dictionaries items
ANY = -1
ASK = -2

SERVICE_TYPE_CONSULTATION = 2
REGION_KRAKOW = 202
CLINIC_KRAKOW_BORA_KOMOROWSKIEGO = 12830
CLINIC_KRAKOW_JASNOGORSKA = 27944

# values used as input
ANY_LITERAL = "ANY"
ASK_LITERAL = "ASK"

# defaults
DEFAULT_REGION = REGION_KRAKOW
DEFAULT_SPECIALIZATION = ASK_LITERAL
DEFAULT_CLINIC = ASK_LITERAL
DEFAULT_DOCTOR = ASK_LITERAL
DEFAULT_AFTER_HOUR = 8
DEFAULT_BEFORE_HOUR = 22
DEFAULT_CHECK_INTERVAL_IN_SEC = 30

# extra variables
debug = True
debug_content = True
authenticated = False
local_timezone = timezone("Europe/Warsaw")
http_med_session = requests.Session()
http_med_session.headers.update({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'})

def show_debug(session_response):
    if session_response.status_code != 200:
        print("\t * ERR CODE: " + str(session_response.status_code))
        exit(1)
    if debug:
        if session_response.history:
            for histreq in session_response.history:
                print("\tPrevious status   :\t {}".format(histreq.status_code))
                print("\tPrevious URL      :\t {}".format(histreq.url))
                print("\tPrevious Headers  :\t {}".format(histreq.headers))
                if debug_content:
                    print("\tPrevious Content  :\t {}".format(histreq.text))
                print("\n")
        print("URL                    :\t {}".format(session_response.request.url))
        print("URL method             :\t {}".format(session_response.request.method))
        print("Request headers        :\t {}".format(session_response.request.headers))
        print("Previous status code   :\t {}".format(session_response.history))
        print("Current status code    :\t {}".format(session_response.status_code))
        print("Current http headers   :\t {}".format(session_response.headers))
        if debug_content:
            print("Page content           :\t {}".format(session_response.text))
        print("-------------------------------------------------------------------\n\n")


def authenticate(login, password):
    global authenticated
    if not authenticated:
        print("[INFO] Logging in...")

        # Open login page
        response = http_med_session.get('https://mol.medicover.pl/Users/Account/LogOn')
        bs = BeautifulSoup(response.text, 'html.parser')
        token = str(bs.script)[217:308]  # i know ugly

        # Send credentials
        response = http_med_session.post(response.url, data={
            'username': login,
            'password': password,
            'idsrv.xsrf': token})

        response.raise_for_status()
        bs = BeautifulSoup(response.content, 'html.parser')

        def get_hidden_field(name):
            return bs.select('input[name="%s"]' % name)[0]['value']

        # Forward data to main page
        response = http_med_session.post(
            'https://mol.medicover.pl/Medicover.OpenIdConnectAuthentication/Account/OAuthSignIn', data={
                'code': get_hidden_field('code'),
                'id_token': get_hidden_field('id_token'),
                'scope': get_hidden_field('scope'),
                'state': get_hidden_field('state'),
                'session_state': get_hidden_field('session_state')})

        response.raise_for_status()

        print("[INFO] Successfully logged in")
        authenticated = True


def open_my_visits():
    search_page_url = "https://mol.medicover.pl/MyVisits" \
                      "?regionId=-2&bookingTypeId=2&specializationId=9&serviceId=&clinicId=-1&languageId=-1&doctorId=-1" \
                      "&searchSince=2018-10-02T02%3A00%3A00.000Z&periodOfTheDay=0" \
                      "&isSetBecauseOfPcc=false" \
                      "&isSetBecausePromoteSpecialization=false" \
                      "&selectedSpecialties="

    print("* INF: Open MyVisits page")
    response = http_med_session.get("https://mol.medicover.pl/MyVisits", allow_redirects=True)
    show_debug(response)

    soup = BeautifulSoup(response.content, 'html.parser')
    spans = soup.find_all('span', attrs={'class': 'username'})
    if spans[1].string != "":
        print("* INF: You are logged as {}".format(spans[1].string))
    else:
        print("* ERR: None user first and last name")
        exit(1)

    print("* INF: Open Booking for specialist")
    response = http_med_session.get("https://mol.medicover.pl/MyVisits?bookingTypeId=2&specializationId=9&pfm=1",
                                    allow_redirects=True)
    show_debug(response)

    print("* INF: Open Search Page")
    response = http_med_session.get(search_page_url, allow_redirects=True)
    show_debug(response)


def search_visit(start_date, end_date, after_hour, before_hour, region, service, clinic, doctor=ANY):
    visits_table = []
    current_date = local_timezone.localize(datetime.datetime.now())
    start_date = local_timezone.localize(datetime.datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S"))
    end_date = local_timezone.localize(datetime.datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S"))
    after_hour = datetime.datetime.strptime(str(after_hour), "%H")
    before_hour = datetime.datetime.strptime(str(before_hour), "%H")

    if debug:
        print("Actual date: {}".format(current_date))
        print(" Start date: {}".format(start_date))
        print("   End date: {}".format(end_date))

    if current_date > start_date:
        start_date = current_date + datetime.timedelta(minutes=20)
        if debug:
            print("Start date not actual, correcting to: {}".format(str(start_date.strftime("%Y-%m-%dT%H:%M:%S"))))

    if current_date > end_date:
        print("Sorry, it`s already after end_date")
        return 0

    response = http_med_session.post(
        "https://mol.medicover.pl/api/MyVisits/SearchFreeSlotsToBook?language=pl-PL",
        headers={"Accept": "application/json"},
        data={"regionIds": [region],
              "serviceTypeId": SERVICE_TYPE_CONSULTATION,
              "serviceIds": [service],
              "clinicIds": [clinic],
              "doctorLanguagesIds": [],
              "doctorIds": [doctor],
              "searchSince": start_date.strftime("%Y-%m-%dT%H:%M:%S.000Z")})

    show_debug(response)
    response_json = json.loads(response.text)

    for x in (response_json['items']):
        visit_id = x['id']
        appointment_date = x['appointmentDate'] # 2019-07-15T15:45:00
        clinic_name = x['clinicName']
        doctor_name = x['doctorName']
        correct_appointment = regex.sub(r'(\S+T\S+[\+|\-]\d+):(\d+)', r"\1\2", appointment_date)
        visit_date = local_timezone.localize(datetime.datetime.strptime(correct_appointment, "%Y-%m-%dT%H:%M:%S"))
        if (visit_date < end_date) and (visit_date.time() >= after_hour.time()) and (
                visit_date.time() <= before_hour.time()):
            visits_table.append({'id': visit_id, 'date': appointment_date, 'place': clinic_name, 'doctor': doctor_name})
            if debug:
                print("\t FOUND APPOINTMENT DATE: {}".format(appointment_date))

    return visits_table


def book_visit(visit):
    response_process = http_med_session.get("https://mol.medicover.pl/MyVisits/Process/Process?id=" + visit['id'],
                                            allow_redirects=True)
    show_debug(response_process)

    # parse response and search visit id
    soup = BeautifulSoup(response_process.content, 'html.parser')
    form_to_confirm = {}

    for inputs in soup.find_all('input'):
        form_to_confirm.update({inputs['name']: inputs['value']})

    response_confirm = http_med_session.post("https://mol.medicover.pl/MyVisits/Process/Confirm", data=form_to_confirm,
                                             allow_redirects=True)
    show_debug(response_confirm)


def get_initial_filters_data(login, password):

    authenticate(login, password)
    http_med_session.get("https://mol.medicover.pl/MyVisits", allow_redirects=True)

    dictionary_url = "https://mol.medicover.pl/api/MyVisits/SearchFreeSlotsToBook/GetInitialFiltersData"
    response = http_med_session.get(dictionary_url.format(), headers={"Accept": "application/json"})
    return json.loads(response.content.decode('utf-8'))


def get_filters_data(login, password, region=ASK, service_type=SERVICE_TYPE_CONSULTATION, service=ASK, clinic=ASK):

    authenticate(login, password)
    http_med_session.get("https://mol.medicover.pl/MyVisits", allow_redirects=True)

    dictionary_url = "https://mol.medicover.pl/api/MyVisits/SearchFreeSlotsToBook/GetFiltersData" \
                     "?regionIds={}&serviceTypeId={}&serviceIds={}&clinicIds={}" \
                     "&searchSince=Sun%20Jul%2014%202019%2014:37:01%20GMT+0200%20(Central%20European%20Summer%20Time)"
    response = http_med_session.get(dictionary_url.format(region, service_type, service, clinic),
                                headers={"Accept": "application/json"})
    return json.loads(response.content.decode('utf-8'))


def get_dictionary_items(data, dictionary_name):
    print("[INFO] Retrieving [{}] dictionary".format(dictionary_name))
    content = data[dictionary_name]
    result = dict()
    for it in content:
        result["{} ({})".format(it['text'], it['id'])] = it['id']
    return result


def get_regions(login, password):
    data = get_initial_filters_data(login, password)
    return get_dictionary_items(data, "regions")


def get_services(login, password, region):
    data = get_filters_data(login, password, region=region)
    return get_dictionary_items(data, "services")


def get_clinics(login, password, region, service):
    data = get_filters_data(login, password, region=region, service=service)
    return get_dictionary_items(data, "clinics")


def get_doctors(login, password, region, specialization, clinic):
    data =  get_filters_data(login, password, region=region, service=specialization, clinic=clinic)
    return get_dictionary_items(data, "doctors")


def interactive(args):
    print("[INFO] Interactive mode")

    class NotEmptyValidator(Validator):
        def validate(self, document):
            if not document.text or len(document.text) == 0:
                raise ValidationError(
                    message='Value must not be empty',
                    cursor_position=len(document.text))

    class CardNumberValidator(Validator):
        def validate(self, document):
            ok = regex.match('^[0-9]{7}$', document.text)
            if not ok:
                raise ValidationError(
                    message='Please enter a valid card number (7 digits)',
                    cursor_position=len(document.text))

    class DateValidator(Validator):
        def validate(self, document):
            ok = regex.match('^[0-9]{4}-[0-9]{2}-[0-9]{2}$', document.text)
            if not ok:
                raise ValidationError(
                    message='Please enter a valid date in format YYYY-MM-DD',
                    cursor_position=len(document.text))

    current_date = local_timezone.localize(datetime.datetime.now()).strftime("%Y-%m-%d")
    if not args.login:
        args.login = prompt([{
            'message': 'Enter card number:',
            'type': 'input',
            'name': 'value',
            'validate': CardNumberValidator}])["value"]

    if not args.password:
        args.password = prompt([{
            'message': 'Enter password:',
            'type': 'password',
            'name': 'value',
            'validate': NotEmptyValidator}])["value"]

    if not args.date_from:
        args.date_from = prompt([{
            'message': 'Enter date from:',
            'type': 'input',
            'name': 'value',
            'default': current_date,
            'validate': DateValidator}])["value"]

    if not args.date_to:
        args.date_to = prompt([{
            'message': 'Enter date to:',
            'type': 'input',
            'name': 'value',
            'default': args.date_from,
            'validate': DateValidator}])["value"]

    if not args.region or args.region == ASK:
        dictionary = get_regions(args.login, args.password)
        args.region = prompt([{
            'message': 'Choose region:',
            'type': 'list',
            'name': 'value',
            'default': 0,
            'choices': list(dictionary.keys()),
            'filter': lambda val: dictionary[val]}])["value"]

    if not args.specialization or args.specialization == ASK:
        dictionary = get_services(args.login, args.password, args.region)
        args.specialization = prompt([{
            'message': 'Choose specialization:',
            'type': 'list',
            'name': 'value',
            'default': 0,
            'choices': list(dictionary.keys()),
            'filter': lambda val: dictionary[val]}])["value"]

    if not args.clinic or args.clinic == ASK:
        dictionary = get_clinics(args.login, args.password, args.region, args.specialization)
        args.clinic = prompt([{
            'message': 'Choose clinic:',
            'type': 'list',
            'name': 'value',
            'default': 0,
            'choices': list(dictionary.keys()),
            'filter': lambda val: dictionary[val]}])["value"]

    if not args.doctor or args.doctor == ASK:
        dictionary = get_doctors(args.login, args.password, args.region, args.specialization, args.clinic)
        args.doctor = prompt([{
            'message': 'Choose doctor:',
            'type': 'list',
            'name': 'value',
            'default': 0,
            'choices': list(dictionary.keys()),
            'filter': lambda val: dictionary[val]}])["value"]


def validate(args):
    print("[INFO] Validating arguments...")

    try:
        if args.region == ANY or args.region == ASK:
            raise ValueError
        regions = get_regions(args.login, args.password)
        list(regions.values()).index(args.region)
    except ValueError:
        raise ValueError("Invalid region ({})".format(args.region))

    try:
        specializations = get_services(args.login, args.password, args.region)
        if args.specialization == ANY or args.specialization == ASK:
            raise ValueError
        list(specializations.values()).index(args.specialization)
    except ValueError:
        raise ValueError("Invalid specialization ({})".format(args.specialization))

    if args.clinic != ANY:
        try:
            if args.clinic == ASK:
                raise ValueError
            clinics = get_clinics(args.login, args.password, args.region, args.specialization)
            list(clinics.values()).index(args.clinic)
        except ValueError:
            raise ValueError("Invalid clinic ({})".format(args.clinic))

    if args.doctor != ANY:
        try:
            if args.doctor == ASK:
                raise ValueError
            doctors = get_doctors(args.login, args.password, args.region, args.specialization, args.clinic)
            list(doctors.values()).index(args.doctor)
        except ValueError:
            raise ValueError("Invalid doctor ({})".format(args.doctor))

    print("[INFO] Validation completed successfully")


# Main program
if __name__ == '__main__':

    def to_date(value):
        if not regex.match('^[0-9]{4}-[0-9]{2}-[0-9]{2}$', value):
            raise argparse.ArgumentTypeError("Invalid date (%s). Should be in YYYY-MM-DD format." % value)
        return value

    def to_hour(value):
        if value.isdigit() and 0 <= int(value) <= 24:
            return int(value)
        raise argparse.ArgumentTypeError("Invalid value (%s). Should be in [0, 24] range." % value)

    def to_id_without_any(value):
        if value == ASK_LITERAL or (value.isdigit() and int(value) > 0):
            return ASK if value == ASK_LITERAL else int(value)
        raise argparse.ArgumentTypeError("Invalid value (%s). Positive or %s expected." % value, ASK_LITERAL)

    def to_id(value):
        if value == ANY_LITERAL or value == ASK_LITERAL or (value.isdigit() and int(value) > 0):
            return ASK if value == ASK_LITERAL else ANY if value == ANY_LITERAL else int(value)
        raise argparse.ArgumentTypeError("Invalid value (%s). Positive number, %s or %s expected." % value, ANY_LITERAL, ASK_LITERAL)

    parser = argparse.ArgumentParser()
    parser.add_argument("--interactive", help="Interactive mode", required=False, action="store_true")
    parser.add_argument("--validate", help="Validate arguments", required=False, action="store_true")
    parser.add_argument("--login", help="Your Medicover login", required='--interactive' not in sys.argv)
    parser.add_argument("--password", help="Your Medicover password", required='--interactive' not in sys.argv)
    parser.add_argument("--date_from", help="Enter the start date of the search period e.g 2018-09-10", type=to_date, required='--interactive' not in sys.argv)
    parser.add_argument("--date_to", help="Enter the end date of the search period e.g 2018-09-12", type=to_date, required='--interactive' not in sys.argv)
    parser.add_argument("--region", help="Region (look README)", type=to_id_without_any, default=DEFAULT_REGION)
    parser.add_argument("--clinic", help="Clinic (look README)", type=to_id, default=DEFAULT_CLINIC)
    parser.add_argument("--specialization", help="Doctor specialization (look README)", type=to_id_without_any, required='--interactive' not in sys.argv)
    parser.add_argument("--doctor", help="Doctor (look README)", type=to_id, default=DEFAULT_DOCTOR)
    parser.add_argument("--after_hour", help="Search visits after an hour", type=to_hour, default=DEFAULT_AFTER_HOUR)
    parser.add_argument("--before_hour", help="Search visits before an hour", type=to_hour, default=DEFAULT_BEFORE_HOUR)
    parser.add_argument("--check_interval", help="Sleep between searches in secs", type=int, default=DEFAULT_CHECK_INTERVAL_IN_SEC)

    args = parser.parse_args()

    if args.interactive:
        interactive(args)

    if args.validate:
        validate(args)

    # TODO: Based on doctor's working hours we could check if dates and hours are valid.

    args.date_to = args.date_to + "T23:59:59"
    args.date_from = args.date_from + "T00:00:00"

    # enter main page and log in
    print("--------------------------------------------------")
    print("Log in to the systems...")
    authenticate(args.login, args.password)

    # open my visits bookmark
    print("Navigate to booking page...")
    open_my_visits()

    # search visits in a loop (check interval)
    print("Search for visits according to your preference...")
    print("Params: --interactive --validate --login {} --date_from {} --date_to {} --after_hour {} --before_hour {} "
          "--region {} --specialization {} --clinic {} --doctor {} --check_interval {}\n"
          .format(args.login,
                  local_timezone.localize(datetime.datetime.strptime(args.date_from, "%Y-%m-%dT%H:%M:%S")).strftime("%Y-%m-%d"),
                  local_timezone.localize(datetime.datetime.strptime(args.date_to, "%Y-%m-%dT%H:%M:%S")).strftime("%Y-%m-%d"),
                  args.after_hour,
                  args.before_hour,
                  args.region,
                  args.specialization,
                  ANY_LITERAL if args.clinic == ANY else args.clinic,
                  ANY_LITERAL if args.doctor == ANY else args.doctor,
                  args.check_interval))

    while True:
        # request to api and return possible visits
        found_visit_list = search_visit(args.date_from,
                                        args.date_to,
                                        args.after_hour,
                                        args.before_hour,
                                        args.region,
                                        args.specialization,
                                        args.clinic,
                                        args.doctor)
        print('.', flush=True, end='')

        # quit when not found and we are after end_date
        if found_visit_list == 0:
            break

        # normal search
        if found_visit_list:
            print("\nBooking:\n",
                  "\t Date: {}\n".format(found_visit_list[0]['date']),
                  "\t Doctor: {}\n".format(found_visit_list[0]['doctor']),
                  "\t Place: {}\n".format(found_visit_list[0]['place']))
            book_visit(found_visit_list[0])
            break

        # interval between searches
        time.sleep(int(args.check_interval))

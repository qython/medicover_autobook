Medicover Auto Registration Script
======

This script should be used to register for a medicover appointment. All you need to do is set your preferred date range, preferred time and doctor's specialty.
Scripts move on the medicover website and search in loop for the best appointment. The first term found is chosen. After finding the preferred visit, script will register you.

Author and License
=====
Author: pyton \
License: I don`t know yet 

ToDo:
=====
 * Check exit code for final request
 * Add more preferences
 * Add option to prefer outpost
 * Add option to prefer before hour
 * Add other locations (currently Cracow is hardcoded)
 * Catch exceptions


Usage:
=====
```bash
python3 medi_autobook.py \
 --date_from 2018-11-08 \
 --date_to 2018-11-20 \
 --specialization 9 \
 --after_hour=18 \
 --before_hour=19 \
 --login=ENTER_YOUR_LOGIN \
 --password=ENTER_YOUR_PASSWORD \
 --check_interval=40
```
Sample above try to book your visit between 2018-11-08 - 2018-11-20 to Internista. Only visits after 18:00 and before 19:00 
will be consider. It will query medicover API every 40 seconds.
 
Notice: ! DO NOT USE **check_interval < 5** OTHERWISE MEDICAL BLACK OPS SECRET SERVICE KNOCK TO YOUR DOOR !


Specialization ID:
=====

 2 - Chirurg dorośli 

 3 - Dermatolog dorośli 

 5 - Endokrynolog dorośli 

 9 - Internista 

 10 - Kardiolog dorośli 

 16 - Neurolog dorośli 

 24 - Psychiatra dorośli 

 25 - Psycholog dorośli 

 26 - Reumatolog dorośli 

 30 - Urolog dorośli 

 33 - Nefrolog dorośli 

 41 - Pulmonolog dorośli 

 42 - Gastroenterolog dorośli 

 54 - Hepatolog 

 76 - Chirurg dziecięcy 

 78 - Chirurg naczyniowy 

 83 - Ortopeda dziecięcy 

 88 - Alergolog dziecięcy 

 96 - Logopeda dorośli 

 99 - Diabetolog 

 100 - Laryngolog dziecięcy 

 112 - Higienistka stomatologiczna 

 124 - Alergolog dziecięcy odczulanie 

 132 - Pediatra - dzieci zdrowe 

 149 - Medycyna podróży 

 158 - Pediatra - dzieci chore 

 163 - Ortopeda dorośli 

 176 - Alergolog dorośli 

 178 - Alergolog dorośli odczulanie 

 182 - Dietetyk dorośli 

 192 - Laryngolog dorośli 

 194 - Logopeda dziecięcy 

 198 - Okulista dorośli 

 200 - Okulista dziecięcy 

 212 - Pulmonolog dziecięcy 

 486 - Internista - szczepienie grypa 

 1188 - Dermatolog dziecięcy 

 1190 - Choroby zakaźne dorośli 

 1586 - Medycyna Rodzinna - dorośli 

 1588 - Medycyna Rodzinna - dzieci zdrowe 

 1590 - Medycyna Rodzinna - dzieci chore 

 1986 - Medycyna Sportowa 

 4150 - Medycyna Rodzinna - szczepienie grypa (dzieci zdrowe) 

 4256 - Medycyna Rodzinna - szczepienie grypa (dorośli) 

 4798 - Ginekolog dorośli 

 4800 - Ginekolog - prowadzenie ciąży 

 4806 - Ginekolog dziecięcy 

 5030 - Internista - szczepienie 

 6254 - Cytologia 

 8168 - Angiolog 

 8474 - USG ciąży 4D 

 9180 - Medycyna Rodzinna - szczepienia (dorośli) 

 9974 - Medycyna podróży - dzieci 

 9976 - Medycyna podróży - dorośli 

 11682 - Badanie dna oka - dorośli 

 16234 -   Medicover Express - przeziębienie, grypa 

 16236 -  Medicover Express - szczepienia dorośli 

 16844 - Poradnia bólu pleców 

 19046 - Internista - porada telefoniczna 

 19048 - Pediatra - porada telefoniczna 

 19054 - Okulistyka/Optometria - dobór okularów 

 22890 - Stomatolog - przegląd 

 23504 - Stomatolog - leczenie 

 27158 - Poradnia Układu Ruchu - dorośli 

 27962 - Endokrynolog - porada telefoniczna 

 27964 - Medycyna rodzinna - dorośli - porada telefoniczna 

 28760 - Chirurg stomatolog - leczenie 

 28762 - Chirurg szczękowo-twarzowy - leczenie 

 28764 - Endodonta - leczenie 

 28766 - Ortodonta - leczenie 

 28768 - Periodontolog - leczenie 

 28770 - Specjalista protetyki stomatologicznej - leczenie 

 28772 - Stomatolog dziecięcy - leczenie 

 29386 - Położna - porada laktacyjna 

 29394 - Implantolog - leczenie 

